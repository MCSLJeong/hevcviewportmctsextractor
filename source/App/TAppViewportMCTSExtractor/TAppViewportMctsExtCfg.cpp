/* The copyright in this software is being made available under the BSD
* License, included below. This software may be subject to other third party
* and contributor rights, including patent rights, and no such rights are
* granted under this license.
*
* Copyright (c) 2010-2017, ITU/ISO/IEC
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
*  * Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*  * Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
*    be used to endorse or promote products derived from this software without
*    specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
* THE POSSIBILITY OF SUCH DAMAGE.
*/

/** \file     TAppMctsExtCfg.cpp
\brief    MCTS Extractor configuration class
*/
#include <cstdio>
#include <cstring>
#include <string>
#include "TAppViewportMctsExtCfg.h"
#include "TAppCommon/program_options_lite.h"

#ifdef WIN32
#define strdup _strdup
#endif

using namespace std;
namespace po = df::program_options_lite;

#if MCTS_EXTRACTION
//! \ingroup TAppMctsExtract
//! \{

// ====================================================================================================================
// Public member functions
// ====================================================================================================================

/** \param argc number of arguments
\param argv array of arguments
*/

template <class T>
struct SMultiValueInput
{
	const T              minValIncl;
	const T              maxValIncl;
	const std::size_t    minNumValuesIncl;
	const std::size_t    maxNumValuesIncl; // Use 0 for unlimited
	std::vector<T> values;
	SMultiValueInput() : minValIncl(0), maxValIncl(0), minNumValuesIncl(0), maxNumValuesIncl(0), values() { }
	SMultiValueInput(std::vector<T> &defaults) : minValIncl(0), maxValIncl(0), minNumValuesIncl(0), maxNumValuesIncl(0), values(defaults) { }
	SMultiValueInput(const T &minValue, const T &maxValue, std::size_t minNumberValues = 0, std::size_t maxNumberValues = 0)
		: minValIncl(minValue), maxValIncl(maxValue), minNumValuesIncl(minNumberValues), maxNumValuesIncl(maxNumberValues), values() { }
	SMultiValueInput(const T &minValue, const T &maxValue, std::size_t minNumberValues, std::size_t maxNumberValues, const T* defValues, const UInt numDefValues)
		: minValIncl(minValue), maxValIncl(maxValue), minNumValuesIncl(minNumberValues), maxNumValuesIncl(maxNumberValues), values(defValues, defValues + numDefValues) { }
	SMultiValueInput<T> &operator=(const std::vector<T> &userValues) { values = userValues; return *this; }
	SMultiValueInput<T> &operator=(const SMultiValueInput<T> &userValues) { values = userValues.values; return *this; }

	T readValue(const TChar *&pStr, Bool &bSuccess);

	istream& readValues(std::istream &in);
};

template <class T>
static inline istream& operator >> (std::istream &in, SMultiValueInput<T> &values)
{
	return values.readValues(in);
}

template<>
UInt SMultiValueInput<UInt>::readValue(const TChar *&pStr, Bool &bSuccess)
{
	TChar *eptr;
	UInt val = strtoul(pStr, &eptr, 0);
	pStr = eptr;
	bSuccess = !(*eptr != 0 && !isspace(*eptr) && *eptr != ',') && !(val<minValIncl || val>maxValIncl);
	return val;
}

template<>
Int SMultiValueInput<Int>::readValue(const TChar *&pStr, Bool &bSuccess)
{
	TChar *eptr;
	Int val = strtol(pStr, &eptr, 0);
	pStr = eptr;
	bSuccess = !(*eptr != 0 && !isspace(*eptr) && *eptr != ',') && !(val<minValIncl || val>maxValIncl);
	return val;
}

template<>
Double SMultiValueInput<Double>::readValue(const TChar *&pStr, Bool &bSuccess)
{
	TChar *eptr;
	Double val = strtod(pStr, &eptr);
	pStr = eptr;
	bSuccess = !(*eptr != 0 && !isspace(*eptr) && *eptr != ',') && !(val<minValIncl || val>maxValIncl);
	return val;
}

template<>
Bool SMultiValueInput<Bool>::readValue(const TChar *&pStr, Bool &bSuccess)
{
	TChar *eptr;
	Int val = strtol(pStr, &eptr, 0);
	pStr = eptr;
	bSuccess = !(*eptr != 0 && !isspace(*eptr) && *eptr != ',') && !(val<Int(minValIncl) || val>Int(maxValIncl));
	return val != 0;
}

template <class T>
istream& SMultiValueInput<T>::readValues(std::istream &in)
{
	values.clear();
	string str;
	while (!in.eof())
	{
		string tmp; in >> tmp; str += " " + tmp;
	}
	if (!str.empty())
	{
		const TChar *pStr = str.c_str();
		// soak up any whitespace
		for (; isspace(*pStr); pStr++);

		while (*pStr != 0)
		{
			Bool bSuccess = true;
			T val = readValue(pStr, bSuccess);
			if (!bSuccess)
			{
				in.setstate(ios::failbit);
				break;
			}

			if (maxNumValuesIncl != 0 && values.size() >= maxNumValuesIncl)
			{
				in.setstate(ios::failbit);
				break;
			}
			values.push_back(val);
			// soak up any whitespace and up to 1 comma.
			for (; isspace(*pStr); pStr++);
			if (*pStr == ',')
			{
				pStr++;
			}
			for (; isspace(*pStr); pStr++);
		}
	}
	if (values.size() < minNumValuesIncl)
	{
		in.setstate(ios::failbit);
	}
	return in;
}

Bool TAppViewportMctsExtCfg::parseCfg(Int argc, TChar* argv[])
{
	Bool do_help = false;
	SMultiValueInput<Int> cfg_targetMCTSIdx(0, std::numeric_limits<Int>::max(), 0, std::numeric_limits<Int>::max());

	po::Options opts;
	opts.addOptions()
		("help", do_help, false, "this help text")
		("c", po::parseConfigFile, "configuration file name")
		("InputBitstreamFile,i", m_inputBitstreamFileName, string(""), "Input bitstream file name")
		("OutputBitstreamFile,b", m_outputBitstreamFileName, string(""), "Output subbitstream file name")
		("TargetMCTSIdx,d", cfg_targetMCTSIdx, cfg_targetMCTSIdx, "Array containing target MCTS idx to be extracted. ")
		;

	po::setDefaults(opts);
	po::ErrorReporter err;
	const list<const TChar*>& argv_unhandled = po::scanArgv(opts, argc, (const TChar**)argv, err);

	for (list<const TChar*>::const_iterator it = argv_unhandled.begin(); it != argv_unhandled.end(); it++)
	{
		fprintf(stderr, "Unhandled argument ignored: `%s'\n", *it);
	}

	if (argc == 1 || do_help)
	{
		po::doHelp(cout, opts);
		return false;
	}

	if (m_inputBitstreamFileName.empty())
	{
		fprintf(stderr, "No input file specified, aborting\n");
		return false;
	}

	// Set tile idx. 
	for (UInt i = 0; i < cfg_targetMCTSIdx.values.size(); i++)
	{
		m_targetMctsIdx.push_back(cfg_targetMCTSIdx.values[i]);
	}
	return true;
}
#endif

//! \}
